# GithubProfileViewer

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 12.1.2. This project is the solution of the task given by TheHouseMonk.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

